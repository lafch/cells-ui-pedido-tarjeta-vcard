import { LitElement, html, css } from 'lit-element';
import { getComponentSharedStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import styles from './cells-ui-pedido-tarjeta-vcard-styles.js';
import '@vcard-components/cells-theme-vcard';
import '@vcard-components/cells-util-behavior-vcard';
import '@bbva-web-components/bbva-form-number';
import '@vcard-components/cells-ui-autocomplete-vcard';
import '@bbva-web-components/bbva-button-action';
import '@bbva-web-components/bbva-link';
import '@bbva-web-components/bbva-button-default';

const cellsUtilBehaviorVcard = CellsBehaviors.cellsUtilBehaviorVcard;

/**
This component ...

Example:

```html
<cells-ui-pedido-tarjeta-vcard></cells-ui-pedido-tarjeta-vcard>
```

##styling-doc

* @customElement
* @polymer
* @LitElement
* @demo demo/index.html
*/
const utilVcard = CellsBehaviors.cellsUtilBehaviorVcard;
export class CellsUiPedidoTarjetaVcard extends utilVcard(LitElement) {
  static get is() {
    return 'cells-ui-pedido-tarjeta-vcard';
  }

  // Declare properties
  static get properties() {
    return {
      user: { type: Object },
      oficina: { type: Object },
      tarjetas: { type: Array },
      solicitud: { type: Object },
      imagenTarjeta: { type: Object },

      isVisibleSolicitud: { type: Boolean },
      estiloCardBody: { type: String },
      estiloCardBodyStyle: { type: String },
      estiloMsjAlerta: { type: String },
      msjAlerta: { type: String },
      valueTarjeta: { type: String },

      cantidadProductos: { type: Number },
      msjListado: { type: String },
      textoAgregar: { type: String },
      cantidadAgregada: { type: String },

      selectedTarjeta: { type: Number },
      selectedFormato: { type: Number },
      valueCantidad: { type: Number },
      idSolicitudItem: { type: Number },

      urlCreditCardNone: { type: String },

      isUpdate: { type: Boolean }

    };
  }

  // Initialize properties
  constructor() {
    super();

    this.estiloCardBody = 'ancho-form-50';
    this.estiloCardBodyStyle = 'ancho-form-65';

    this.tarjetas = [];
    this.solicitud = new Object();
    this.solicitud.detalleSolicitud = [];
    this.formato = '--';
    this.imagenTarjeta = new Object();
    this.estiloMsjAlerta = 'visible-false';
    this.msjAlerta = '';

    this.isVisibleCatalogo = false;
    this.isVisibleSolicitud = true;
    this.selectedTarjeta = null;
    this.selectedFormato = 0;
    this.valueCantidad = 0;
    this.idSolicitudItem = 0;
    this.cantidadAgregada = '';

    this.textoAgregar = 'Agregar';
    this.msjListado = 'No tiene ningún producto agregado.';
    this.cantidadProductos = 0;

    this.urlCreditCardNone = '';
    this.isUpdate = false;

    this.addEventListener('clear-value-autocomplete',async (event) => {
      this.selectedTarjeta = null;  
      await this.requestUpdate();
    });

  }

  verDetalleSolicitud() {
    this.isVisibleSolicitud = false;
  }

  regresarSolicitud() {
    this.isVisibleSolicitud = true;
  }

  solicitarPedido() {
    console.log('Envio de Solicitud:', this.solicitud);
    this.dispatch(this.events.solicitarPedidoEvent, this.solicitud);
  }

  async validarDatosIngreso() {
    var data = new Object();
    var tarjeta = await this.selectedTarjeta;
    var cantidad = this.getInputValue('txtCantidad');
    console.log(tarjeta)
    if (this.isEmpty(tarjeta)) {
      this.msjAlerta = ' Seleccione una tarjeta.';
    } else {
      this.msjAlerta = '';
      data.tarjeta = tarjeta;
      let rango = this.extract(tarjeta, 'rango', 1);
      if (!this.validarCantidad(cantidad, rango)) {
        this.msjAlerta = ' La cantidad ingresada debe ser multiplo de ' + rango;
      } else {
        this.msjAlerta = '';
        data.cantidad = cantidad;
      }
    }
    return data;
  }

  validarCantidad(cantidadSolicitada, rango) {
    var cantidad = parseInt(cantidadSolicitada);
    if (Number.isInteger(cantidad)) {
      if (cantidad > 0) {
        let valor = cantidad % rango;
        if (valor == 0) {
          return true;
        } else {
          return false
        }
      } else {
        return false;
      }
    } else {
      return false;
    }

  }

  async agregarTarjeta() {
    var data = await this.validarDatosIngreso();
    if (data && data.tarjeta && data.cantidad) {
      if (this.idSolicitudItem === 0) {
        let solicitudItem = new Object();
        solicitudItem._id = data.tarjeta._id;
        solicitudItem.tarjeta = data.tarjeta;
        solicitudItem.cantidad = data.cantidad;
        if (!this.validarDuplicado(solicitudItem)) {
          this.solicitud.detalleSolicitud.push(solicitudItem);
          this.cantidadAgregada = this.solicitud.detalleSolicitud.length;
          this.msjAlerta = '';
        } else {
          this.msjAlerta = 'El producto que desea agregar ya se encuentra en el detalle de pedido.';
        }
      } else {
        let index = this.buscarItemSolicitudId(this.idSolicitudItem, this.solicitud);
        if (index > -1) {
          this.solicitud.detalleSolicitud[index].tarjeta = data.tarjeta;
          this.solicitud.detalleSolicitud[index].cantidad = data.cantidad;
          this.verDetalleSolicitud();
        }
      }

      this.limpiarDatos();
    }
  }

  eliminarPedido(solicitudItem) {
    let index = this.buscarItemSolicitudId(solicitudItem._id, this.solicitud);
    if (index > -1) {
      this.solicitud.detalleSolicitud.splice(index, 1);
    }
    this.cantidadAgregada = this.solicitud.detalleSolicitud.length;
  }

  validarDuplicado(item) {
    if (this.solicitud && this.solicitud.detalleSolicitud.length > 0) {
      for (var index in this.solicitud.detalleSolicitud) {
        if (this.solicitud.detalleSolicitud[index].tarjeta._id == item.tarjeta._id) {
          return true;
        }
      }
      return false;
    } else {
      return false;
    }
  }

  buscarItemSolicitudId(id, solicitud) {
    if (solicitud && solicitud.detalleSolicitud.length > 0) {
      for (var index in solicitud.detalleSolicitud) {
        if (solicitud.detalleSolicitud[index]._id == id) {
          return index;
        }
      }
      return -1;
    } else {
      return -1;
    }
  }

  async setVisibleStep1(visible) {
    this.isVisibleSolicitud = visible;
    await this.requestUpdate();
  }

  async setSolicitud(solicitudDto) {
    this.isUpdate = true;
    this.solicitud = solicitudDto;
    this.solicitud.detalleSolicitud = solicitudDto.details;
    this.cantidadAgregada = solicitudDto.details.length;
    await this.requestUpdate();
  }

  resetForm() {
    this.isUpdate = false;
    this.isVisibleSolicitud = true;
    this.solicitud = {};
    this.solicitud.detalleSolicitud = [];
    this.cantidadAgregada = '';
    this.limpiarDatos();
  }

  limpiarDatos() {
    this.valueTarjeta = '';
    this.msjAlerta = '';
    this.valueCantidad = 0;
    this.idSolicitudItem = 0;
    this.textoAgregar = 'Agregar';
    this.selectedTarjeta = null;
    this.imagenTarjeta = null;
    if (this.shadowRoot.querySelector('cells-ui-autocomplete-vcard')) {
      this.shadowRoot.querySelector('cells-ui-autocomplete-vcard').clearValue();
    }
    if (this.shadowRoot.querySelector('#txtCantidad')) {
      this.shadowRoot.querySelector('#txtCantidad').value = '';
    }
    this.requestUpdate();
  }

  async editarPedido(solicitudItem) {
    this.regresarSolicitud();
    await this.requestUpdate;
    if (solicitudItem) {
      if (solicitudItem.tarjeta._id) {
        this.selectedTarjeta = this.indexSelectedId(solicitudItem.tarjeta._id, this.tarjetas);
        this.imagenTarjeta = this.selectedTarjeta.imagen;
      }

      if (typeof solicitudItem.tarjeta.value !== 'undefined') {
        this.shadowRoot.querySelector('cells-ui-autocomplete-vcard').setValue(solicitudItem.tarjeta.value);
      }

      if (typeof solicitudItem.cantidad !== 'undefined') {
        this.shadowRoot.querySelector('#txtCantidad').value = solicitudItem.cantidad;
        this.valueCantidad = solicitudItem.cantidad;
      }
      if (typeof solicitudItem._id !== 'undefined') {
        this.textoAgregar = 'Editar';
        this.idSolicitudItem = solicitudItem._id;
      }
    }

  }

  indexSelectedNombre(nombre, lista) {
    for (var item in lista) {
      if (lista[item].nombre == nombre) {
        return item;
      }
    }
  }

  indexSelectedId(id, lista) {
    let tmp = lista.filter((item) => {
      return item._id === id;
    });
    return tmp && tmp.length > 0 ? tmp[0] : null;
  }

  async onChangeTarjeta(e) {
    console.log('onChangeTarjeta', e);
    this.selectedTarjeta = await e.detail;
    this.formato = this.selectedTarjeta.formato;
    this.imagenTarjeta = await this.selectedTarjeta.imagen;
    await this.requestUpdate();
  }

  htmlEstiloTarjeta(imagenTarjeta) {
    if (this.selectedTarjeta && this.imagenTarjeta) {
      return html`
            <div class='request-card-solicitud-body-style ${this.estiloCardBodyStyle}'>
              <div class='contenedor'>
                  <div class='contenedor-scroll'>
                      <div class='styleCard'>
                        <div class='styleCard-body'>
                          <img src='${this.imagenTarjeta}' width='80%'>
                        </div>
                      </div>
                  </div>
              </div>
            </div>`;
    } else {
      /*
      return html`
          <div class="post">
            <div class="avatar"></div>
            <div class="line"></div>
            <div class="line"></div>
          </div>

          <div class="post">
            <div class="avatar"></div>
            <div class="line"></div>
            <div class="line"></div>
          </div>

          <div class="post">
            <div class="avatar"></div>
            <div class="line"></div>
            <div class="line"></div>
          </div>
      `;
      */
      return html`
     <div class='request-card-solicitud-body-style ${this.estiloCardBodyStyle}'>
              
              <div class='contenedor'>
                  <div class='contenedor-scroll'>
                      <div class='styleCard'>
                        <div class='styleCard-body '>
                          <img class ="no-card-view" src="${this.urlCreditCardNone}"  />
                        </div>
                      </div>
                  </div>
              </div>
            </div>`;
    }

  }

  htmlListadoPedido() {
    if (this.solicitud && this.solicitud.detalleSolicitud.length > 0) {
      return this.solicitud.detalleSolicitud.map(solicitudItem =>
        html`
              <div class='item-pedido'>
                <div class='item-detalle'> 
                  <div> <span class='sub-titulo'> ${solicitudItem.tarjeta && solicitudItem.tarjeta.value ? solicitudItem.tarjeta.value : ""} </span> </div>
                  <div><span> ${solicitudItem.tarjeta && solicitudItem.tarjeta.formato ? solicitudItem.tarjeta.formato : ""} </span></div>
                  <div><span class='badge badge-primary badge-pill'> ${solicitudItem.cantidad}<span></div>
                </div>
                <div class='item-imagen'>
                  <img src='${solicitudItem.tarjeta.imagen ? solicitudItem.tarjeta.imagen : this.urlCreditCardNone}' width='50px' height='40px'>
                </div>
                <div class='item-action'>
                  <bbva-link icon="coronita:edit"  class='action-pedido' @click='${() => { this.editarPedido(solicitudItem); }}'></bbva-link>
                  <bbva-link icon="coronita:trash" class='action-pedido' @click='${() => { this.eliminarPedido(solicitudItem); }}'></bbva-link>
                </div>
              </div>	
              `);
    } else {
      return html`<div class='contenedor-mensaje'><label>${this.msjListado}</label></div>`;
    }
  }

  htmlMensajeAlerta() {
    if (this.msjAlerta !== '') {
      return html`<div class='error'>
        <cells-icon icon='coronita:info'></cells-icon>
        <p>${this.msjAlerta}</p>
      </div>`;
    } else {
      return '';
    }
  }

  get getCodigoOficina() {
    return this.oficina && this.oficina.codigo ? this.oficina.codigo : '';
  }

  get getNombreOficina() {
    return this.oficina && this.oficina.nombre ? this.oficina.nombre : '';
  }

  get getNombreCompleto() {
    return this.user && this.user.nombreCompleto ? this.user.nombreCompleto : '';
  }


  static get shadyStyles() {
    return `
      ${styles.cssText}
      ${getComponentSharedStyles('cells-ui-pedido-tarjeta-vcard-shared-styles').cssText}
      ${getComponentSharedStyles('cells-theme-vcard').cssText}
    `;
  }

  changeValueTarjeta(event) {
    console.log('changeValueTarjeta', event.detail.value);
    if (this.isEmpty(event.detail.value)) {
      this.selectedTarjeta = null;
      this.requestUpdate();
    }

  }


  // Define a template
  render() {
    return html`
      <style>${this.constructor.shadyStyles}</style>
      <div class='container-pedido'>
        ${this.isVisibleSolicitud ? html`
        <div class='request-card-solicitud'>
          <div class='request-card-solicitud-header'>
            <div class='contenedor-datos-solicitante'>
              <div>
                <label class="titulo">Datos de solicitante  </label>
              </div>
              <div class='margin-top-1'>
                <label class="subTitulo">Nombre Completo:  </label> <label class = "uppercase">${this.getNombreCompleto}</label>
              </div>
              <div class='margin-top-1'>
                <label class="subTitulo">Oficina: </label> <label>${this.getCodigoOficina} - ${this.getNombreOficina}</label>
              </div>
            </div>
            <div class='contenedor-ver-detalle'>
              <bbva-button-action icon='coronita:visualize' notifications="${this.cantidadAgregada}" label="Ver detalle pedido" @click='${this.verDetalleSolicitud}'></bbva-button-action>
            </div>
        </div>
          <div class='request-card-solicitud-body margin-top-2'>
            <div class='request-card-solicitud-body-form ${this.estiloCardBody}'>
              <div>
                <label class="titulo">Datos de solicitud</label>
              </div>
              <div class='margin-top-3'>
                <label class="subTitulo">Tipo de tarjeta</label>
              </div>
              <div class='margin-top-1' style = "margin-right: 2%;">
                <cells-ui-autocomplete-vcard 
                              id="cmbTarjeta" 
                              @value-changed = "${this.changeValueTarjeta}"
                              @selected-item="${this.onChangeTarjeta}"
                              label="Tarjeta seleccionada"
                              .items = ${this.tarjetas}  
                              displayLabel="value"
                              displayValue="_id">
                </cells-ui-autocomplete-vcard>
              </div>
              <div class='margin-top-3'>
                <label class="subTitulo">Formato de tarjeta</label>
              </div>
              <div class='margin-top-1' >
                <label class="">${this.formato}</label>
              </div>
              <div class='margin-top-3' style = "margin-right: 2%;">
                <bbva-form-number  id='txtCantidad' value = ""  label='Cantidad de tarjetas' ></bbva-form-number>
              </div>
            </div>
            ${this.htmlEstiloTarjeta(this.imagenTarjeta)}
            ${this.htmlMensajeAlerta()}
            <div class='request-card-solicitud-footer'>
              <bbva-button-default id='btnAgregar'     @click='${this.agregarTarjeta}' text='${this.textoAgregar}'></bbva-button-default>
              <bbva-button-default id='btnVerDetalle'  @click='${this.verDetalleSolicitud}' text='Ver detalle pedido'></bbva-button-default>
            </div>
          </div>
        </div>` : html`
        <div class='request-card-detalle'>
          <div class='request-card-detalle-header'>
            <div class='contenedor-titulo-listado'>
              <label class='titulo'>Listado de productos</label>
            </div>
            <div class='contenedor-botonera-listado'>
              <bbva-button-action icon='coronita:return-12' label="" @click='${this.regresarSolicitud}'></bbva-button-action>
            
              </div>
              
          </div>

          <div class='lst-pedido'>
              ${this.htmlListadoPedido()}
              <div class='request-card-detalle-footer'>
                <bbva-button-default id='btnSolicitar'  @click='${this.solicitarPedido}' text='${this.isUpdate ? 'Modificar Solicitud' : 'Solicitar'}'></bbva-button-default>
              </div>
            </div>
          
          </div>
          `}
        </div>
      </div>
    `;
  }
}

// Register the element with the browser
customElements.define(CellsUiPedidoTarjetaVcard.is, CellsUiPedidoTarjetaVcard);
