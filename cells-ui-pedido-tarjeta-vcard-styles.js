import { css, } from 'lit-element';


export default css`:host {
  display: block;
  box-sizing: border-box;
  font-size: 0.9rem;
  @apply --cells-ui-pedido-tarjeta-vcard; }

:host([icon]) > .action-pedido {
  --bbva-link-icon: {
    color:blue;
  }

; }

:host([hidden]), [hidden] {
  display: none !important; }

*, *:before, *:after {
  box-sizing: inherit; }

.container-pedido {
  margin-bottom: 5%; }

.no-card-view {
  width: 255px;
  margin-top: -60px;
  overflow: hidden; }

.uppercase {
  text-transform: uppercase; }

.contenedor-datos-solicitante {
  float: left;
  width: 90%; }

.conteneddor-ver-detalle {
  float: right; }

.titulo {
  color: #004481;
  font-weight: bold;
  font-size: 0.95rem; }

.subTitulo {
  color: #949494; }

.error {
  margin-top: 1%;
  float: left;
  position: relative;
  border-radius: 5px;
  padding: 15px 10px;
  box-sizing: border-box;
  background: #F4C3CA;
  border: 1px solid rgba(244, 195, 202, 0.3);
  width: calc(100% - 20px);
  min-height: 20px;
  overflow: hidden;
  display: flex;
  font-weight: bold;
  --cells-icon-height: 32px;
  --cells-icon-width: 32px;
  align-items: center;
  justify-content: center;
  -webkit-transition: width 2s;
  /* For Safari 3.1 to 6.0 */
  transition: width 2s; }
  .error cells-icon {
    flex: 0 0 60px;
    text-align: center;
    color: #FF3352; }
  .error p {
    margin: 0;
    flex: 0 0 calc(100% - 60px); }

.request-card-detalle {
  display: inline-block;
  width: 100%; }
  .request-card-detalle .item-pedido {
    width: 94%;
    display: inline-block;
    margin-left: 3%;
    padding-top: 2%;
    border-bottom: 1px solid rgba(0, 0, 0, 0.125);
    padding-bottom: 2%; }
  .request-card-detalle .lst-pedido:last-child {
    border-bottom: 0px; }
  .request-card-detalle .item-detalle {
    float: left;
    width: 80%; }
    .request-card-detalle .item-detalle .sub-titulo {
      color: #1973B8;
      font-weight: bold; }
  .request-card-detalle .item-imagen {
    float: left;
    width: 10%; }
  .request-card-detalle .item-action {
    float: left;
    width: 10%;
    line-height: 3; }
  .request-card-detalle .badge-primary {
    color: #fff !important;
    background-color: #4285f4 !important; }
  .request-card-detalle .badge-pill {
    padding-right: .6em;
    padding-left: .6em;
    border-radius: 10rem; }
  .request-card-detalle .badge {
    display: inline-block;
    padding: 0.25em 0.4em;
    font-size: 75%;
    font-weight: 700;
    line-height: 1;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: 0.25rem;
    transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out; }
  .request-card-detalle-header {
    display: inline-block;
    width: 95%;
    margin-left: 3%; }
    .request-card-detalle-header .contenedor-titulo-listado {
      float: left;
      width: 78%;
      display: block;
      line-height: 4; }
    .request-card-detalle-header .contenedor-botonera-listado {
      float: right;
      display: block;
      padding-top: 18px; }
  .request-card-detalle-footer {
    width: 100%;
    text-align: center;
    margin-top: 1%;
    float: left; }
  .request-card-detalle .contenedor-mensaje {
    margin-left: 3%;
    margin-top: 3%;
    margin-bottom: 3%;
    font-weight: bold;
    text-align: center; }

.request-card-solicitud-header {
  width: 95%;
  margin-left: 3%;
  margin-top: 3%;
  margin-bottom: 1%; }

.request-card-solicitud-body {
  width: 95%;
  display: inline-block;
  margin-left: 3%; }

.request-card-solicitud-footer {
  width: 100%;
  text-align: center;
  margin-top: 3%;
  float: left; }

.request-card-solicitud-body-plus {
  border-right: 1px solid;
  margin-right: 3%; }

@media (min-width: 750px) {
  .request-card-solicitud .ancho-form-50 {
    width: 60%;
    float: left;
    display: block;
    margin-right: 3%;
    padding-right: 2%;
    padding-bottom: 2%;
    border-right: solid 1px;
    border-color: #949494; }
  .request-card-solicitud .ancho-form-65 {
    line-height: 2;
    width: 37%;
    float: left; }
  .request-card-solicitud .ancho-form-90 {
    width: 90%; }
  .request-card-solicitud .contenedor-scroll {
    display: flex;
    width: 100%;
    overflow: auto;
    align-items: center;
    justify-content: center; }
  .request-card-solicitud .margin-top {
    margin-top: 1%; }
  .request-card-solicitud .styleCard {
    text-align: center;
    display: inline-block; } }

@media (max-width: 768px) {
  .request-card-solicitud .post {
    width: 260px; }
  .request-card-solicitud .post .line {
    width: 190px; }
  .request-card-solicitud .no-card-view {
    width: 260px;
    margin-top: -60px; } }

.request-card-solicitud bbva-button-default {
  margin-bottom: 1%; }

@media (max-width: 320px) {
  .request-card-solicitud .post {
    width: 280px; }
  .request-card-solicitud .post .line {
    width: 210px; }
  .request-card-solicitud .no-card-view {
    margin-top: -70px; }
  .request-card-solicitud .ancho-form-50 {
    width: 100%;
    float: left;
    display: block; }
  .request-card-solicitud .ancho-form-65 {
    width: 100%;
    float: left;
    display: block; }
  .request-card-solicitud .ancho-form-60 {
    width: 100%;
    float: left; }
  .request-card-solicitud .ancho-form-100 {
    width: 100%; }
  .request-card-solicitud .contenedor-scroll {
    text-align: center; }
  .request-card-solicitud .margin-top {
    margin-top: 10%; }
  .request-card-solicitud .margin-top-style {
    margin-top: 10%; }
  .request-card-solicitud .styleCard {
    margin: 5%;
    text-align: center; } }

.request-card-solicitud .margin-top-3 {
  margin-top: 3%; }

.request-card-solicitud .margin-top-2 {
  margin-top: 2%; }

.request-card-solicitud .margin-top-1 {
  margin-top: 1%; }

.request-card-solicitud .styleCard-body {
  text-align: center;
  margin-left: 2%; }

.post {
  float: left;
  width: 340px;
  height: 80px; }

.post .avatar {
  float: left;
  width: 52px;
  height: 52px;
  background-color: #ccc;
  border-radius: 25%;
  margin: 8px;
  background-image: linear-gradient(90deg, #F4F4F4 0px, rgba(229, 229, 229, 0.8) 40px, #F4F4F4 80px);
  background-size: 600px;
  animation: shine-avatar 2s infinite ease-out; }

.post .line {
  float: left;
  width: 260px;
  height: 16px;
  margin-top: 12px;
  border-radius: 7px;
  background-image: linear-gradient(90deg, #F4F4F4 0px, rgba(229, 229, 229, 0.8) 40px, #F4F4F4 80px);
  background-size: 600px;
  animation: shine-lines 2s infinite ease-out; }

.post .avatar + .line {
  margin-top: 11px; }

.post .line ~ .line {
  background-color: #ddd; }

@keyframes shine-lines {
  0% {
    background-position: -100px; }
  40%, 100% {
    background-position: 140px; } }

@keyframes shine-avatar {
  0% {
    background-position: -32px; }
  40%, 100% {
    background-position: 208px; } }
`;